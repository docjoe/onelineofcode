---
title: €/LOC
separator: <!--s-->
verticalSeparator: <!--v-->
theme: kommitment
theme-override : theme/kommitment.css
revealOptions:
    slideNumber: 'c/t'
    width: 1280
    height: 800
---



# Was kostet eine Line Of Code - <br>wissen wir, was wir tun?

Johannes Mainusch
2018-02-05


<br>
<br>
<br>
This is a true story. The events depicted in this presentation took place in IT in the last years. At the request of the survivors, the names have been changed. Out of respect for the dead, the rest has been told exactly as it has happened...
<!-- .element: class="fragment djCitation" data-fragment-index="2" -->

<!--v-->
<!-- .slide: data-background="./img/OOP2018-abstract.png" -->

Note:
# Abstract

<img src="./img/OOP2018-abstract.png" width="2000px">


Fragt man Entwickler, lauten die Erwiderungen:
Lines of Code sind eh eine schlechte Metrik?
Wichtiger wäre es, die gelöschten Lines of Code zu bewerten!
Fragt man Marketing-Menschen, hört man:
Was ist eine Line of Code? Warum ist das interessant? Zu teuer!
Fragt man Theater-Leute hört man:
Eine Line of Koks?
Immerhin sind Lines of Code das, was wir produzieren. Da ich vom mangelnden Wissen um die einfachsten wirtschaftlichen Fragen stark befremdet bin, werde ich sie in diesem Vortrag beleuchten und einige Einblicke geben.

Extended Abstract
Coden, wissen wir eigentlich noch, was das ist? Wer von uns checkt denn überhaupt noch etwas? Wer checkt etwas ein?
Es ist Zeit, einen Rant über die Blindheit zu schreiben, mit der wir unterwegs sind. Was wird denn passieren, wenn der letzte COBOL-Entwickler ins Gras gebissen hat? Warum verwendet VW Logistiksysteme, von denen sie den Source-Code vergessen haben? Warum kann Britisch-Airways keine Passagiere befördern, wenn die Systeme ausfallen? Und warum fallen die überhaupt aus? Wieso sitzen in den Vorstandsetagen großer Firmen so wenig IT-Leute? Verstehen wir überhaupt noch, was wir tun? Haben wir einen großen Babel-Turm gebaut? Wie viele Lines of Code kann ein Mensch pflegen? Wie viele eine Organisation? Warum investieren wir nicht mehr in die IT-Ausbildung von Menschen?
Wenn man googelt, was eine LOC kostet, so kommen Antworten von 40c bis 5€. Die Zeile Code auf den T-Shirts der OOP 2016 (Details Matter) war bestimmt viel teurer. Ebenso die Vertauschung von Punkt und Komma einer Schnittstelle im Abrechnungssystem bei einem großen sozialen Netzwerk 2008, die dazu führte, dass wir Ende Februar 18 Mio. € von 1800 Kunden-Kreditkarten einzogen. Usw. usw.
Zeit, einen Vortrag dazu zu erarbeiten.




<!--s-->
<!-- .slide: data-background="#f9d658" style="font-size: 3em"-->
# Warum?
Note: 
Warum mich das interessiert, kann ich am Beispiel meiner ersten Wochen als CTO bei der EPOST in erklären. Da musste ich sehr schnell einen Überblick über Systeme, Standorte, Deployment, Entwickler, Betrieb und Anforderungen bekommen. Mein Fazit nach kurzer Zeit: eine Line of Code war hier t3r...
<!--v-->
<!-- .slide: data-background="#f9d658" style="font-size: 3em"-->
# Promised
<!--v-->
<!-- .slide: data-background="./img/_BonnBerlin01.jpg" -->
<!--v-->
<!-- .slide: data-background="#f9d658" style="font-size: 3em"-->
# Reality
<!--v-->
<!-- .slide: data-background="./img/_BonnBerlin02.jpg" -->
Note:
![](./img/_BonnBerlin02.jpg)
<!--v-->
<!-- .slide: data-background="#f9d658" -->

	~150 Mitarbeiter, >50% extern, 
	etwa 80 Entwickler
	sehr hohe Fluktuation
	3 Standorte
	~18 Teams (dauernde Änderungen)
	~300 Production Bugs
	~70 Komponenten (no Readme.md)

Fazit:

	keine Zuordnung von 
	Teams --> Produkten
	Komponenten --> Produkten
	Komponenten --> Teams
	shared everything!

Note:
Das war wie eine Flugzeugwartung ohne Betriebsmittel-Inventar

<!--v-->
<!-- .slide: data-background="#f9d658" -->
<!-- .slide: data-background="./img/KomponentenMonsterQuartett.png" -->

Note:
![](./img/KomponentenMonsterQuartett.png)

- Wie viele Komponenten gibt es?
- Wie heißen die?
- Welchen Zweck haben die?
- Welches Team ist verantworlich?
- Welche Technologie?
- Wie groß (LoC)?

<!--v-->
<!-- .slide: data-background="#f9d658" style="font-size: 3em" -->
# ist das normal?

<!--v-->
<!-- .slide: data-background="#f9d658" style="font-size: 3em" -->
<!-- .slide: data-background="./img/utf8-U+1F640.png" -->
Note:
![](./img/utf8-U+1F640.png)
JA!
&#x1F640;


<!--s-->
# so I asked...
<!-- .element: style="font-size:6em;	" -->
<!--v-->

![](./img/twitterOLOC.png)
<!-- .element: style="height:900px;  " -->
![](./img/LoCUmfrage.png)
<!-- .element: style="height:900px;	" -->

Note:
Die Frage allein führt zu den unterschiedlichsten Reaktionen. Häufig bekommt ich total empfindliche Reaktiuonen. Ich habe dazu eine kleine Umfrage mit google und twitter gestartet. Hier einige der Reaktionen:

<!--v-->

# Unbezahlbar.
Note:
Irgendjemand bezahlt doch was und LoC kommen dabei auch heraus.

<!--v-->
  
# Ist pauschal nicht ermittelbar, da die Dauer der Nutzung, der Verwendungszweck, die Wartbarkeit von Code durch Dritte eine große Rolle spielen.
Note:
Spielen Nutzungsdauer, Verwendugszweck für die Kosten wirklich eine Rolle? Ich glaube nicht. Bei der Wartbarkeit stimme ich sofort zu.

<!--v--> 
# Das Ziel von Software ist es Wert zu schaffen, eine Zeile Code kostet also nicht, sondern schafft Wert.
Note:
Das Software Wert schafft, das finde ich auch. Aber das eine Zeile Code nix kostet, das ist leider nicht so. Open Source das von Entwicklern an die Community mit freien Lizenzen gestiftet wird, halte ich aus diesem Vortrag heraus. Meine Meinung dazu: das schaffrt wirklich nachhaltig Wertn und ich glaube, dadurch wird die Welt ein ganzes Stück besser!

<!--v-->

<!-- .slide: data-background="./img/twitterOLOC-Reaktion01.jpg" -->
Note:
![](./img/twitterOLOC-Reaktion01.jpg)

<!--v-->

# empfindliche
<!-- .element: style="font-size:4.0em;  background-color: rgba(255, 255, 255, 0.75); color: rgba(71, 81, 251, 0.75);" -->

<br><br><br><br><br><br><br><br>

# Reaktionen!
<!-- .element: style="font-size:4em;  background-color: rgba(255, 255, 255, 0.75); color: rgba(71, 81, 251, 0.75);" -->
<!-- .slide: data-background="./img/stepOnToe.jpg" -->
Note:
![](./img/stepOnToe.jpg)
<!--v-->
# €/LOC 
**TABU?**
<!-- .element: class="fragment" style="font-size:10em; color: red;" -->
Note:
Ist die Frage nach LoC an sich schon ein Tabu? Und wie groß also wird das Sakrileg, wenn  man nach den Kosten pro LoC fragt?

<!--v-->
# Antworten<br><br><br><br><br><br><br>
<!-- .element: style="color: #3e67c5;" -->
komisch, dass niemand glaubt, 10€ - 20€...
<!-- .element: class="fragment" style="color: #3e67c5;" -->

<!-- .slide: data-background="./img/LoCUmfrageAntwort.png" -->

Note:
![](./img/LoCUmfrageAntwort.png)
https://docs.google.com/forms/d/1dE6qKkow6xACI8lN2sP7fSfRbFdXXxr5ugNlZ8QUkDE/edit#responses








<!--s-->
# €/LOC Beispiel 1

<!--v-->





	c beware of the punctuation mark
		DO 3 I = 1.3P	
<!-- .element: class="fragment" -->	  
  
	c is not 
		DO 3 I = 1,3P
<!-- .element: class="fragment" -->
<div>
OOP 2017: Details Matter T-shirt
<br>
[https://www5.in.tum.de/lehre/...](https://www5.in.tum.de/lehre/seminare/semsoft/unterlagen_02/erdfern/website/mariner.html)
<br>
[https://en.wikipedia.org/wiki/Mariner_1](https://en.wikipedia.org/wiki/Mariner_1)
</div>
<!-- .element: class="fragment djCitation" -->
<!-- .slide: data-background="./img/detailsMatterTshirt.png" -->

<!--v-->

# 1962: &#36;18.5 million lost 
in Mariner 1 Venus Mission 
by 1 LOC	

<br>
OK, this may not be a true story. Read wikipedia for details...
<!-- .element: class="fragment" -->

<!--s-->
# Bugs
<!-- .element: class="djLabel-left" -->
<!-- .slide: data-background="./img/zecke.jpg" -->

<!--v-->

<!-- .slide: data-background="./img/dateLang.png" -->

Note:
![](./img/dateLang.png)
Die beiden Bugs, denen ich immer wieder begegne, sind unterschiedliche Ausprägungen des Language-Bugs und des Timezoze-Bugs. Beide haben gemeinsam, dass an einer Systemgrenze, der Kontext unterschiedlich von zwei Kommunikationspartnern interpretiert wird.

<!--v-->
# €/LOC Beispiel 2
<!-- .element: style="font-size: 5em;"-->
<!-- .slide: data-background=" #367775	"-->
	
<!--v-->
# 2008
<!-- .slide: data-background=" #367775	"-->
- &#35;HamburgSocialnetwork
<!-- .element: style="font-size: 2em;"-->
- &#35;newJobsProduct
<!-- .element: style="font-size: 2em;"-->
- &#35;1800JobCustomers
<!-- .element: style="font-size: 2em;"-->
- &#35;hugeItComany@Gütersloh
<!-- .element: style="font-size: 2em;"-->
- &#35;18Mio€=Jahresumsatz
<!-- .element: style="font-size: 2em;"-->

Note:
A young php developer implemendet a host interfache (#HamburgSocialnetwork, #newJobsProduct, #1800JobCustomers, #hugeItComany@Gütersloh, #18Mio€=Jahresumsatz, )


<!--v-->
# 2008
<!-- .slide: data-background="#367775" -->

	<?php
	$money = 790.23;
	$val = sprintf("%01.2f", $money);
	// echo $val gibt "790.23" aus
	// das Backend erwartet 790,23€
	// und macht 79023€ daraus...
	?>

<!--v-->
<!-- .slide: data-background=" #367775	"-->
# 1LOC = 400.000€
Note:
# Bugs sind teuer
Das war teuer! ca. 1800 Customer anrufen, Vertrag nachverhandeln, PR-Eindämmungsffeld aufbauen... Ich schätze etwa 500 PT ≈ 400.000€. Hätte übler ausgehen können...

<!--s-->

# Beispiel 3
# XING E/LOC 2008



<!--s-->
# §266 HGB	
<!-- .element: style="font-size: 9em" -->

Note:
Unter bestimmten Bedingungen muss Software als immaterielles Wirtschaftsgut bewertet und in die Bilanz aufgenommen werden.

<!--v-->
<!-- .slide: data-background="./img/zalando2016BilanzIV.png" -->
	Zalando bewertet den Zugang 
	immaterieller Vermögenswerte 
	aus der Aktivierung von Software 
	auf über 20Mio € in 2016.
<!-- .element: class="fragment" -->	

![](./img/Zalando-SE_logo.png)
<!-- .element: style="position: absolute; right: -40px; top: -250px	"-->

Note:
![](./img/zalando2016BilanzIV.png)
Zalando bewertet den Zugang immaterieller Vermögenswerte aus der Aktivierung von Software auf über 20Mio € in 2016.

<!--v-->
# <br><br><br><br><br><br>Unternehmen kennen den Wert von Software!
<!-- .slide: data-background="./img/QOTSA02.jpg" -->
Note:
![](./img/QOTSA02.jpg)
<!--v-->

Note:
<!-- .slide: data-background="./img/theManager.png" -->
![](./img/theManager.png)

<!--v-->

	# Lebenszyklus von Software
	[Heinrich 2002, S. 237]

<!-- .element: class="fragment"  -->

<!-- .slide: data-background="./img/SW-Lebenszyklus01.png" -->
Note:
![](./img/SW-Lebenszyklus01.png)
http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Management-von-Anwendungssystemen/Lebenszyklus-von-Anwendungssystemen/index.html
und
[Heinrich 2002, S. 237]

+++ 
# XING E/LOC 2016
zur Berechnung nehmen wir 
- XING 2008 300.000 LOC bei 40 developer (7,5k/Dev) 
- XING 2016 300 Entwickler ==> 2,25 MIO LOC
==> ~25€/LOC


+++
# profit is opinion
# cash is fact!















# Rechtslage

# § 266 Abs. 8 HGB
<!--v-->



<!--s-->
# Warum mich das interessiert

<!--v-->

hier vielleicht erst die EPOST Geschichte



<!--s-->



<!--v-->

# Schaden != Wert
Titel: was **kostet** eine Line of Code
<br><br><br><br><br><br><br><br><br><br><br><br>
<!-- .slide: data-background="./img/EgyptianPounds.jpg" -->
Note:
![](./img/EgyptianPounds.jpg)

<!--v-->





# EPOST
- EPOST CTO Geschichte
- picue Softwareübernahme Geschichte
- Bank Softwareübernahme Geschichte
- VW Logistik Geschichte

Immer die Frage: Wie kann ich den Status Quo beurteilen...

Ich möchte es einfach reparieren können... Wir mein Fahrrad.

# XING 2008
300.000LOC / 40 Engineers
==> 1 Engineer hat 7.500LOC

XING: 2017 mit etwa 500 Entwicklern hat demnach etwa
7500*500/40 = 3,75Mio LOC
Software Wert lauf Bilanz (intagible assest) etwa 60MIO€
--> 16€ pro LOC

# 2014 Berliner Entwicklungsfirma

Note:
18 Teams, Drei Standorte, etwa 70-90 Softwarekomponenten und alles gut durchmischt...
>300 Tickets in Jira, 2 Wochen Sprints, etwa 5-10 Tickets/Woche abgearbeitet, ebensoi viele Neu 
d.h. 12 Monate Backlog aus Tickets...


# Die XING/Twitter Umfrage

Note:
Ich habe dann auf XING und Twitter gefragt...


# Zwischenfazit

Note: 


Das ist mir zu unordentlich! Ich möchte es genauer wissen...





# Idee 1: die Bilanz
Warum wird Software nicht jährlich bilanziert und im Jahresbericht eines Unternehmens ausgewiesen?

# Idee 2: Lines of Code werden mit dem Alter immer teurer 
- denn der Ersteller hat ja noch die Erinnerung an die ERstellung im Kopf
- die Teammitglieder kennen das Entwicklungs- und Produktsetup
- Schon bei der ersten Übergabe muss neu gelernt und gelesen werden. Tests, Readmes und automatisiertes Deploment hilft hier
- eine Community hilft hier (bspw. JQUERY oder Go)
- irgendwann sind auch die Skills der Programmierer nicht mehr so vorhanden (COBOL, Simula, Angular1)

Fallbeispiel Bank Risikomanagementsoftware:

# Idee 3: warum interessiert mich das
- EPOST CTO Geschichte
- Bank Softwareübernahme Geschichte
- picue Softwareübernahme Geschichte

Immer die Frage: Wie kann ich den Status Quo beurteilen...

ich möchte es einfach reparieren können... Wir mein Fahrrad.

# Idee 4: Warum einfach Neuschreiben oft nicht geht...
Beispiel Bank oder picue...

# idee 5: Agilität und Bilanzrecht
http://www.dashoefer.de
War das der Grund, warum wir die Glücksspielsoftware aud dem EPOST_Kernel nicht entfernen durften. War zu zu erwartende Abschreibung zu hoch?

# Idee 5: Zahlen Daten Fakten
- DevSpeed: LOC/TAG/Entwickler: 50-100 (10)
- Was ist LOC (alles, was gesdchrieben wurde)
- Verbosität von Sprachen: JAVA, FORTRAN, GO, BASH, README
- billig: hello World (ist das wirklich so billig, -framework, Installation, lesen...)?
- teuer: Password Change in Bank Microservices
- Übergaben: Wer hat schon Software übergeben, wer übernommen...
	- Junior: liest sehr viel (Stackoverflow) pro LOC
	- Advanced: spricht Sprache udn framework fließend
	- Senior: pairt, wird zum Junior in der nächsten Sprache, spricht mit Menschen
- Vergleich von Projekten (picue android, kontrol, kontrol-frondend, IRIS, irgendetwas Otto...)
	- LOC:
	- dLOC: deleted lines of code
	- nLOC: new lines of code
	- Committer:
- Wie viel kostet eine Entwicklerstunde?
- wie viele Entwickler gibt es in Deutschland?
- how is our GNLOC?
- Wird AI und retten? --> Nein

	
# Bildungslücken
- IRIS: sendmail geht ohne user/passwort
- JWT ist keine Alternative zu cookies
- 	



# Erfolgreiche Software lebt länger



# Software muss übergeben werden, oder gehört abgeschaltet
--> sonst entstehen, Zombies

# Auf Übergaben einstellen

# Fixing Code in Production
--> is expensive
Beispiel Bank

# Why Pair Programming
Pair Programming ist die erste Code-Übergabe!

# Why Test driven?
--> einfache Übergaben

# Why Readmes
--> einfache Übergabe

# Why designated standing teams
--> einfach Übergabe

# Antipattern - Software nach Indien übergeben
(kein Antipattern wäre demnach die Erstellung von Software in Indien)

# Antipattern- Entwickler verlieren
--> Brain Drain erschwert Übergabe

# Pro-Pattern - Abschaltung vorbereiten
--> einfache und dokumentierte Schnittstellen
--> CDC-Tests
--> isolierte Datenhaltung (share nothing)
--> kleinere Services (beware of the Microservice Myth)



<!--s-->
# Was nun?
# Software Bilanz!

<!--v-->


    $ cat README.md

&nbsp; 

    # Name: oneLineOfCode
    # modified: 5.2.2018
    # Zweck:   über die Kosten von Code
    # Team:    Johannes Mainusch et al.
    # Tech-Stack: reveal.md
    # run:     reveal-md oneLineOfCode.md 
    # LOC/am:	 486/2018-01-13
    #
    #
    

<!--s-->
<!-- .slide: data-background="./img/MonsterQuartett01.png" -->
# Fazit

	- LOC kostet von 4€ bis 25.000€
	- Weniger ist mehr? 
		Lesbarer ist mehr!
	- macht mal Inventur
		(1xpro Jahr)
	- schreibt Readmeas! 
		(Hört nicht auf Guido)

Note:
![](./img/MonsterQuartett01.png)
<!--v-->
<!-- .slide: data-background="#000000"  -->
# EOF
<!-- .element: style="font-size: 15em; color: #00f900; font-family: monospace;" -->

