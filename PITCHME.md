# Was kostet eine Line Of Code - <br>wissen wir, was wir tun?

Johannes Mainusch
2018-02-06

<br>
<br>
This is a true story. The events depicted in this presentation took place in IT in the last years. At the request of the survivors, the names have been changed. Out of respect for the dead, the rest has been told exactly as it has happened...
<!-- .element: class="fragment djCitation" data-fragment-index="2" -->

+++?image=img/OOP2018-abstract.png
Note:
# Abstract
![](./img/OOP2018-abstract.png)
Fragt man Entwickler, lauten die Erwiderungen:
Lines of Code sind eh eine schlechte Metrik?
Wichtiger wäre es, die gelöschten Lines of Code zu bewerten!
Fragt man Marketing-Menschen, hört man:
Was ist eine Line of Code? Warum ist das interessant? Zu teuer!
Fragt man Theater-Leute hört man:
Eine Line of Koks?
Immerhin sind Lines of Code das, was wir produzieren. Da ich vom mangelnden Wissen um die einfachsten wirtschaftlichen Fragen stark befremdet bin, werde ich sie in diesem Vortrag beleuchten und einige Einblicke geben.

Extended Abstract
Coden, wissen wir eigentlich noch, was das ist? Wer von uns checkt denn überhaupt noch etwas? Wer checkt etwas ein?
Es ist Zeit, einen Rant über die Blindheit zu schreiben, mit der wir unterwegs sind. Was wird denn passieren, wenn der letzte COBOL-Entwickler ins Gras gebissen hat? Warum verwendet VW Logistiksysteme, von denen sie den Source-Code vergessen haben? Warum kann Britisch-Airways keine Passagiere befördern, wenn die Systeme ausfallen? Und warum fallen die überhaupt aus? Wieso sitzen in den Vorstandsetagen großer Firmen so wenig IT-Leute? Verstehen wir überhaupt noch, was wir tun? Haben wir einen großen Babel-Turm gebaut? Wie viele Lines of Code kann ein Mensch pflegen? Wie viele eine Organisation? Warum investieren wir nicht mehr in die IT-Ausbildung von Menschen?
Wenn man googelt, was eine LOC kostet, so kommen Antworten von 40c bis 5€. Die Zeile Code auf den T-Shirts der OOP 2016 (Details Matter) war bestimmt viel teurer. Ebenso die Vertauschung von Punkt und Komma einer Schnittstelle im Abrechnungssystem bei einem großen sozialen Netzwerk 2008, die dazu führte, dass wir Ende Februar 18 Mio. € von 1800 Kunden-Kreditkarten einzogen. Usw. usw.
Zeit, einen Vortrag dazu zu erarbeiten.

+++?image=img/MonsterQuartett01.png
# Fazit

# Fazit

	- LOC kostet von >>4€ bis 25.000€
	~30€ ist die Zahl, mit der ich weiter rechne
<!-- .element: class="fragment" -->
	- Inventur hilft
		(1xpro Jahr)
<!-- .element: class="fragment" -->	
	- Software hat einen Lebenszyklus! 
<!-- .element: class="fragment" -->	
	- Lasst uns unsere Arbeit messbarer machen!
<!-- .element: class="fragment" -->
	- Pair Programming Coach hilft! 
<!-- .element: class="fragment" -->

Note:
![](./img/MonsterQuartett01.png)


---
<!-- .slide: data-background="#f9d658" style="font-size: 2.3em"-->
# Warum?
Note: 
Warum mich das interessiert, kann ich am Beispiel meiner ersten Wochen als CTO bei der EPOST in erklären. Da musste ich sehr schnell einen Überblick über Systeme, Standorte, Deployment, Entwickler, Betrieb und Anforderungen bekommen. Mein Fazit nach kurzer Zeit: eine Line of Code war hier t3r...
+++
<!-- .slide: data-background="#f9d658" style="font-size: 2.3em"-->
# Promised
+++?image=img/_BonnBerlin01.jpg
Note:
![](./img/_BonnBerlin01.jpg)
+++
<!-- .slide: data-background="#f9d658" style="font-size: 2.3em"-->
# Reality
+++?image=img/_BonnBerlin02.jpg
Note:
![](./img/_BonnBerlin02.jpg)
Immer die Frage: Wie kann ich den Status Quo beurteilen...
Ich möchte es einfach reparieren können... Wie mein Fahrrad.
+++
<!-- .slide: data-background="#f9d658" -->
# Analyse (Due Dilligence)
* ~150 Mitarbeiter, >50% extern, 
* etwa 80 Entwickler
* sehr hohe Fluktuation
* 3 Standorte
* ~18 Teams (dauernde Änderungen)
* ~300 Production Bugs
* ~70 Komponenten (no Readme.md)

+++
<!-- .slide: data-background="#f9d658" style="font-size: 2.4em" -->
# ist das normal?

+++?image=img/frowningcat.png&contain no-repeat
<!-- .slide: data-background="#f9d658" style="font-size: 2.4em" -->
Note:
![](./img/frowningcat.png)
JA!
&#x1F640;

+++
<!-- .slide: data-background="#f9d658" -->
# Unordnung!
* keine Zuordnung von 
* Teams --> Produkten
* Komponenten --> Produkten
* Komponenten --> Teams
* shared everything!

Note:
Das war wie eine Flugzeugwartung ohne Betriebsmittel-Inventar.


+++?image=img/KomponentenMonsterQuartett.png
<!-- .slide: data-background="#f9d658" -->
Note:
Hier hätte ich mir ein paar Inventurdaten gewünscht!
- Wie viele Komponenten gibt es?
- Wie heißen die?
- Welchen Zweck haben die?
- Welches Team ist verantworlich?
- Welche Technologie?
- Wie groß (LoC)?

![](./img/KomponentenMonsterQuartett.png)

+++?image=img/chaos01.jpg
# Ordnung schaffen!
<!-- .element: class="fragment"  style="font-size:4em; color: red;	" -->







---
# so I asked...
<!-- .element: style="font-size:4em;	" -->
+++

![](./img/twitterOLOC.png)
<!-- .element: style="height:900px;  " -->
![](./img/LoCUmfrage.png)
<!-- .element: style="height:900px;	" -->

Note:
Die Frage allein führt zu den unterschiedlichsten Reaktionen. Häufig bekommt ich total empfindliche Reaktiuonen. Ich habe dazu eine kleine Umfrage mit google und twitter gestartet. Hier einige der Reaktionen:

+++
# Unbezahlbar.
Note:
Irgendjemand bezahlt doch was und LoC kommen dabei auch heraus.

+++
## Ist pauschal nicht ermittelbar, da die Dauer der Nutzung, der Verwendungszweck, die Wartbarkeit von Code durch Dritte eine große Rolle spielen.
Note:
Spielen Nutzungsdauer, Verwendugszweck für die Kosten wirklich eine Rolle? Ich glaube nicht. Bei der Wartbarkeit stimme ich sofort zu.

+++ 
# Das Ziel von Software ist es Wert zu schaffen, eine Zeile Code kostet also nicht, sondern schafft Wert.
Note:
Das Software Wert schafft, das finde ich auch. Aber das eine Zeile Code nix kostet, das ist leider nicht so. Open Source das von Entwicklern an die Community mit freien Lizenzen gestiftet wird, halte ich aus diesem Vortrag heraus. Meine Meinung dazu: das schaffrt wirklich nachhaltig Wertn und ich glaube, dadurch wird die Welt ein ganzes Stück besser!

+++?image=img/EgyptianPounds.jpg
# Frage verfehlt...
Titel: was **kostet** eine Line of Code
<br><br><br><br><br><br><br><br><br><br><br><br>
Note:
![](./img/EgyptianPounds.jpg)

+++?image=img/twitterOLOC-Reaktion01.jpg
Note:
![](./img/twitterOLOC-Reaktion01.jpg)

+++?image=img/stepOnToe.jpg
# empfindliche
<!-- .element: style="font-size:3.0em;  background-color: rgba(255, 255, 255, 0.75); color: rgba(71, 81, 251, 0.75);" -->
<br><br><br><br><br><br><br>
# Reaktionen!
<!-- .element: style="font-size:3em;  background-color: rgba(255, 255, 255, 0.75); color: rgba(71, 81, 251, 0.75);" -->
Note:
![](./img/stepOnToe.jpg)
+++
# €/LOC 
**TABU?**
<!-- .element: class="fragment" style="font-size:8em; color: red;" -->
Note:
Ist die Frage nach LoC an sich schon ein Tabu? Und wie groß also wird das Sakrileg, wenn  man nach den Kosten pro LoC fragt?

+++?image=img/LoCUmfrageAntwort.png
# Antworten<br><br><br><br><br><br>
<!-- .element: style="color: #3e67c5;" -->
komisch, dass niemand glaubt, 10€ - 20€...
<!-- .element: class="fragment" style="color: #3e67c5;" -->

Note:
![](./img/LoCUmfrageAntwort.png)
https://docs.google.com/forms/d/1dE6qKkow6xACI8lN2sP7fSfRbFdXXxr5ugNlZ8QUkDE/edit#responses








---
# €/LOC Beispiel 1

+++?image=img/detailsMatterTshirt.png&size=contain no-repeat

	c beware of the punctuation mark
		DO 3 I = 1.3P	
<!-- .element: class="fragment" -->	  
  
	c is not 
		DO 3 I = 1,3P
<!-- .element: class="fragment" -->
<div>
OOP 2017: Details Matter T-shirt
<br>
[https://www5.in.tum.de/lehre/...](https://www5.in.tum.de/lehre/seminare/semsoft/unterlagen_02/erdfern/website/mariner.html)
<br>
[https://en.wikipedia.org/wiki/Mariner_1](https://en.wikipedia.org/wiki/Mariner_1)
</div>
<!-- .element: class="fragment djCitation" -->

+++

# 1962: &#36;18.5 million lost 
in Mariner 1 Venus Mission 
by 1 LOC	

<br>
OK, this may not be a true story. Read wikipedia for details...
<!-- .element: class="fragment" -->

---?image=img/zecke.jpg
# Bugs
<!-- .element: style="color: #33201c;left: -1px;top: 500px;font-size: 5em;position: absolute;"-->

+++?image=img/dateLang.png

Note:
![](./img/dateLang.png)
Die beiden Bugs, denen ich immer wieder begegne, sind unterschiedliche Ausprägungen des Language-Bugs und des Timezoze-Bugs. Beide haben gemeinsam, dass an einer Systemgrenze, der Kontext unterschiedlich von zwei Kommunikationspartnern interpretiert wird.





---
# €/LOC Beispiel 2
<!-- .element: style="font-size: 4em;"-->
<!-- .slide: data-background=" #367775	"-->
	
+++
<!-- .slide: data-background=" #367775	"-->
- &#35;HamburgSocialnetwork
<!-- .element: style="font-size: 2em;"-->
- &#35;newJobsProduct
<!-- .element: style="font-size: 2em;"-->
- &#35;1800JobCustomers
<!-- .element: style="font-size: 2em;"-->
- &#35;hugeITComany@Gütersloh
<!-- .element: style="font-size: 2em;"-->
- &#35;18Mio€=Jahresumsatz
<!-- .element: style="font-size: 2em;"-->

Note:
A young php developer implemendet a host interfache (#HamburgSocialnetwork, #newJobsProduct, #1800JobCustomers, #hugeItComany@Gütersloh, #18Mio€=Jahresumsatz, )


+++
<!-- .slide: data-background="#367775" -->

	<?php
	$money = 790.23;
	$val = sprintf("%01.2f", $money);
	// echo $val gibt "790.23" aus
	// das Backend erwartet 790,23€
	// und macht 79023€ daraus...
	?>
+++
<!-- .slide: data-background=" #367775	"-->
# 1LOC = 400.000€
Note:
# Bugs sind teuer
Das war teuer! ca. 1800 Customer anrufen, Vertrag nachverhandeln, PR-Eindämmungsffeld aufbauen... Ich schätze etwa 500 PT ≈ 400.000€. Hätte übler ausgehen können...
Aber eigentlich muss man solch  Bugs ja auch dann auf alle LOC umlegen, um einen Durchschnittswert zu bekommen.



---
<!-- .slide: data-background="#245553" -->
# Beispiel 3
# XING 2008

+++?image=img/XING2008.png
<!-- .slide: data-background="#245553" -->
Note:
Das haben wir damals bei XING in Präsentationen gezeigt. Daraus kann man schon einiges lernen.

+++?image=img/XING2008-01.png
<!-- .slide: data-background="#245553" -->

    // 300.000 LOC
    // % 40 Entwickler
    // = 7500LOC/Entwickler
    
+++
<!-- .slide: data-background="#245553" -->
![](img/XING2008-02.png)
# 35€/LOC






---
# §266 HGB	
<!-- .element: style="font-size: 8em" -->

Note:
Unter bestimmten Bedingungen muss Software als immaterielles Wirtschaftsgut bewertet und in die Bilanz aufgenommen werden. 
Für selbsterstellte Software gibt es ein Aktivierungswahlrecht nach  § 248 Abs. 2 Satz 1 HGB.

+++?image=img/theManager01.png
Note:
![](./img/theManager01.png)

+++?image=img/theNerd01.png
# &nbsp;&nbsp;&nbsp;&nbsp;...und nu?<br><br><br><br><br>
<!-- .element: style="color: brown;" -->
Note:
![](./img/theNerd01.png)

+++?image=img/theManager02.png
Note:
![](./img/theManager02.png)
Achtung, bei Banken müssen diese Anlagen vom Eigenkapital abgezogen werden und können so die von der BaFin verlangte Eigenkapitalquote verletzen!

+++?image=img/che1951.png
Note:
![](./img/che1951.png)
+++?image=img/QOTSA02.jpg
## <br><br><br><br><br>Unternehmen kennen den Wert von Software!
Note:
![](./img/QOTSA02.jpg)


+++?image=img/Zalando2016BilanzIV.png

	/*
	Zalando bewertet den Zugang 
	immaterieller Vermögenswerte 
	aus der Aktivierung von Software 
	auf über 20Mio € in 2016.
	*/
<!-- .element: class="fragment" -->	

![](./img/Zalando-SE_logo.png)
<!-- .element: style="position: absolute; right: -40px; top: -150px; height: 50%	"-->

Note:
![](./img/Zalando2016BilanzIV.png)
Zalando bewertet den Zugang immaterieller Vermögenswerte aus der Aktivierung von Software auf über 20Mio € in 2016.




---
<!-- .slide: data-background="#245553" -->
# Beispiel 4

+++?image=img/XING2016BilanzIV.png
Note:
![](./img/XING2016BilanzIV.png)
XING weist 2016 den Wert ihrer Software auf 56Mio€ aus. Das entspricht etwa den Gehaltskosten für 500 Produktentwickler.
+++ 
# XING E/LOC 2016
zur Berechnung nehmen wir 
- XING 2008 300.000 LOC bei 40 Developer (7,5k/Dev) 
- XING 2016 300 Entwickler ==> 2,25 MIO LOC
- Bilanz sagt: 56Mio€
==> ~25€/LOC





---?image=img/picue.png
    Beispiel 5
    Picue 
    Android Client
<!-- .element: class="fragment fade-out" style="font-size: 1em; color: #ff7700;" -->

+++?image=img/picue01.png
    Kosten:
    500€/Tag (Startup) *
    2,5  Entwickler *
    90 Tage
    /75% Overhead
    = 150.000€
<!-- .element: class="fragment fade-out" style="font-size: 1em; color: #ff7700;" -->

Note:
Daraus habe ich einmal ausgerechnet, was die Erstellung pro LOC gekostet hat...

+++?image=img/picue02.png
    4€/LOC
<!-- .element: class="fragment fade-out" style="font-size: 1em; color: #ff7700;" -->

+++
# Pair-Programming Coach!
+++
# Why Pair Programming
Pair Programming ist die erste Code-Übergabe!
+++
# Aber

---
# Software lebt länger
als Du denkst...

+++?image=img/SW-Lebenszyklus01.png

	# Lebenszyklus von Software
	[Heinrich 2002, S. 237]
<!-- .element: class="fragment fade-out" style="font-size: 1em; color: #ff7700;" -->

<!-- .element: class="fragment"  -->
Note:
![](./img/SW-Lebenszyklus01.png)
http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Management-von-Anwendungssystemen/Lebenszyklus-von-Anwendungssystemen/index.html
und
[Heinrich 2002, S. 237]

+++
# Why Test driven?
--> einfache Übergaben
+++
# Why Readmes
--> einfache Übergabe
+++
# Why designated standing teams
--> einfach Übergabe
+++?image=img/SW-Lebenszyklus01.png


+++
# Beispiel 6
# Das Ende vom Lebenssyklus
Intershop-Abschaltung bei OTTO.
Note:
Aus ASSEMBLER/C/... wurde PL/SQL

+++?image=img/LhotseLOC.png
    27€/LOC
<!-- .element: class="fragment fade-out" style="font-size: 1em; color: #ff7700;" -->


---
# Code lesen

+++
![](https://blog.codinghorror.com/content/images/uploads/2006/09/6a0120a85dcdae970b0120a86d7477970b-pi.png)

[JPHartmans Link: https://blog.codinghorror.com/when-understanding-means-rewriting/](https://blog.codinghorror.com/when-understanding-means-rewriting/)
<!-- .element: class="fragment fade-out" style="font-size: 1em; color: #ff7700;" -->

Note:
https://blog.codinghorror.com/when-understanding-means-rewriting/

+++
# Beispiel 7
System mit 20+ Microservices...

+++?image=img/SoftareBilanz.png
    25.000€/LOC
<!-- .element: class="fragment fade-out" style="font-size: 1em; color: #ff7700;" -->
Note:
Wir haben in den ersten 8 Wochen nur versucht, das System zu verstehen und deployxen zu können. Die Dokumentation war großteils in Jenkins...
Erste richtige Anforderung war es, die Accounts für automatisch versendete Mails mit einem Passwort>20 Stellen zu sichern.
Ergebnis: ca. 2 LOC.
--> 25.000€ pro LOC
Okay, auf die 50.000 LOC des Systems umgelegt, haben wir die Gesamtkosten auch nur um 1€ in die Höhe getrieben...

+++
# Mein Wunsch:

    $ cat README.md

&nbsp; 

    # Name: oneLineOfCode
    # modified: 6.2.2018
    # Zweck:   über die Kosten von Code
    # Team:    Johannes Mainusch et al.
    # Tech-Stack: reveal, Gitpitch
    # run: https://gitpitch.com/docjoe/onelineofcode/master?grs=bitbucket
    # LOC/am:	 486/2018-01-13
    # LOC/am:	2031/2018-02-06
    #

<!-- .element: class="fragment" -->    
Note:
Ich wünsche mir, dass ich schnell einen aktuellen Überblick über den Stand von Komponenten, Teams und der Verantwortungszuordnung von Komponenten zu Teams bekommen kann...

---?image=img/MonsterQuartett01.png
# Fazit

	- LOC kostet von >>4€ bis 25.000€
	~30€ ist die Zahl, mit der ich weiter rechne
<!-- .element: class="fragment" -->
	- Inventur hilft
		(1xpro Jahr)
<!-- .element: class="fragment" -->	
	- Software hat einen Lebenszyklus! 
<!-- .element: class="fragment" -->	
	- Lasst uns unsere Arbeit messbarer machen!
<!-- .element: class="fragment" -->
	- Pair Programming Coach hilft! 
<!-- .element: class="fragment" -->

Note:
![](./img/MonsterQuartett01.png)
+++
<!-- .slide: data-background="#000000"  -->
# EOF
<!-- .element: style="font-size: 13em; color: #00f900; font-family: monospace;" -->


+++?image=img/static_qr_code_without_logo.jpg&size=auto

---?image=img/kommitment.png&size=auto




